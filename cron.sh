#!/bin/bash

set -eu

cd /app/code

echo "=> Run cron tasks"
/usr/local/bin/gosu www-data:www-data php /app/code/admin.php clean_expired
/usr/local/bin/gosu www-data:www-data php /app/code/admin.php clean_async
