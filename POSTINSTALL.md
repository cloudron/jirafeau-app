This app is pre-setup with an admin account. The initial credentials are:

**Username**: (no username)<br/>
**Password**: changeme123<br/>

Please change the admin password immediately by editing the file `custom.php` using
the [File Manager](/filemanager.html?type=app&id=$CLOUDRON-APP-ID).

