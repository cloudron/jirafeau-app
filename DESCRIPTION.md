## About

Jirafeau is a "one-click-filesharing": Select your file, upload, share a link. That's it.

## Main features

* One upload - One download link & one delete link
* Shows progression: speed, percentage and remaining upload time
* Preview content in browser (if possible)
* Optional password protection (for uploading or downloading)
* Set expiration time for downloads
* Option to self-destruct after first download
* Shortened URLs using base 64 encoding
* Maximal upload size configurable
* NO database, only use basic PHP
* Simple language support with a lot of langages (help us on weblate!)
* File level Deduplication for storage optimization (does store duplicate files only once, but generate multiple links)
* Optional data encryption
* Small administration interface
* CLI script to remove expired files automatically with a cronjob
* Basic API
* Bash script to upload files via command line
* Themes
