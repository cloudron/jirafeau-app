#!/bin/bash

set -eu
cd /app/code

echo "=> Ensure directories"
mkdir -p /run/jirafeau/sessions /app/data/var/files /app/data/var/links /app/data/var/async /app/data/media

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

echo "=> Create runtime config"
cat >/run/jirafeau/config.php <<EOL
<?php

\$cfg['web_root'] = '${CLOUDRON_APP_ORIGIN}/';
\$cfg['var_root'] = '/app/data/var/';

\$cfg['proxy_ip'] = array('${CLOUDRON_PROXY_IP}');

\$cfg['installation_done'] = true;
\$cfg['debug'] = false;

include '/app/data/custom.php';
EOL

if [[ ! -f /app/data/custom.php ]]; then
    echo "=> Ensure custom.php"
    cat >/app/data/custom.php <<EOL
<?php

// See https://gitlab.com/mojo42/Jirafeau/-/blob/master/lib/config.original.php for options to overwrite here

// Generate the admin_password value with the following command: echo -n "newpassword" | sha256sum
// Set admin_password to an empty string to disable the admin interface
\$cfg['admin_password'] = '494a715f7e9b4071aca61bac42ca858a309524e5864f0920030862a4ae7589be';

EOL
fi

for theme in `find "/app/code/media.original"/* -maxdepth 0 -type d -printf "%f\n"`; do
    rm -f "/app/data/media/${theme}"
    ln -sf "/app/code/media.original/${theme}" "/app/data/media/${theme}"
done

echo "=> Ensure permissions"
chown -R www-data.www-data /app/data /run/jirafeau

echo "==> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
