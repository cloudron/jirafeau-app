FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
RUN a2disconf other-vhosts-access-log && a2enmod rewrite env
COPY apache/jirafeau.conf /etc/apache2/sites-enabled/jirafeau.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 5G && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 5G && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP memory_limit 512M && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.enable 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.enable_cli 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.interned_strings_buffer 8 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.max_accelerated_files 10000 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.memory_consumption 128 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.save_comments 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini opcache opcache.revalidate_freq 1 && \
    crudini --set /etc/php/8.3/cli/php.ini Session session.save_path /run/jirafeau/sessions && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.save_path /run/jirafeau/sessions && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.3/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

# https://gitlab.com/jirafeau/Jirafeau/-/tags
# renovate: datasource=gitlab-tags depName=jirafeau/Jirafeau versioning=semver
ARG JIRAFEAU_VERSION=4.6.2

RUN curl -LSs https://gitlab.com/jirafeau/Jirafeau/-/archive/${JIRAFEAU_VERSION}/Jirafeau-${JIRAFEAU_VERSION}.tar.gz | tar -xz -C /app/code/ --strip-components 1 -f -

RUN cp /app/code/lib/config.original.php /app/code/lib/config.local.php && \
    echo "require '/run/jirafeau/config.php';" >> /app/code/lib/config.local.php

# https://gitlab.com/jirafeau/Jirafeau/-/tree/master#how-can-i-change-the-terms-of-service
RUN ln -sf /app/data/tos.local.txt /app/code/lib/tos.local.txt 

RUN mv /app/code/media /app/code/media.original && ln -s /app/data/media /app/code/media

ADD start.sh cron.sh /app/pkg/

RUN chown -R www-data.www-data /app/code

CMD [ "/app/pkg/start.sh" ]
